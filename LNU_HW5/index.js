//Task1
function isEquals(a, b){
    return console.log(a === b);
}
//isEquals(3,3);

//Task2
function numToString (num){
    return console.log(num.toString());
}
//numToString(1285);

//Task3
function storeNames(...items){
    return console.log(items);
}
//storeNames('Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy');

//Task4
function getDivision(a, b){
    if(a<b){
        let temp = a;
        a = b;
        b = temp;
    }
    return console.log(a/b);
}
//getDivision(5, 10);
//getDivision(4, 2);

//Task5
function negativeCount (arr){
    let count = 0
    arr.forEach(function(a) {
        if (a < 0) {
            count++;
        }
    });
    return console.log(count);
}
//negativeCount([4, 3, 2, 9]) // => 0
//negativeCount([0, -3, 5, 7]) // => 1